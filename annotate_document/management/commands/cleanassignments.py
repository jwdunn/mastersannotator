from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import now

from annotate_document.models import Profile, DocumentAnnotation, Link


class Command(BaseCommand):
    help = "Unassign old unannotated documents"

    def unassign(self, docs):
        for ann_doc in docs:
            pf = ann_doc.profile
            pf.num_words -= ann_doc.document.words
            if not pf.refresh:
                pf.refresh = True
            pf.save()
            # Update document
            doc = ann_doc.document
            doc.num_annotations -= 1
            doc.save()
            # Remove links
            Link.objects.filter(created_by=pf, mention__document=doc).delete
            # Delete annotation
            ann_doc.delete()

    def handle(self, *args, **options):
        cutoff_doc = now() - timedelta(seconds=settings.DOCUMENT_TTL)
        cutoff_user = now() - timedelta(seconds=settings.USER_ACTIVE)
        oldest_doc = DocumentAnnotation.objects.exclude(completed=True).order_by("assigned_at").first()
        if oldest_doc:
            print(oldest_doc, oldest_doc.assigned_at)
            print(f"Doc cutoff: {cutoff_doc}\n User cutoff {cutoff_user}")

        # Unassign unstarted
        stale_docs = DocumentAnnotation.objects.exclude(annotated_lines__contains='1')\
            .filter(assigned_at__lt=cutoff_doc, profile__last_activity__lt=cutoff_user)
        print(stale_docs.count())
        self.unassign(stale_docs)

        # Unassign partially completed documents
        cutoff_doc = now() - timedelta(seconds=settings.DOCUMENT_INCOMPLETE_TTL)
        print("Partial Cutoff", cutoff_doc)
        stale_partial_docs = DocumentAnnotation.objects.exclude(completed=True).filter(assigned_at__lt=cutoff_doc,
                                                               profile__last_activity__lt=cutoff_user)
        print(stale_partial_docs.count())
        self.unassign(stale_partial_docs)
