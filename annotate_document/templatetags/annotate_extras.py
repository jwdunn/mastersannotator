from django import template
from django.conf import settings
from django.template.defaultfilters import  stringfilter

register = template.Library()


@register.filter(name='count')
@stringfilter
def count(value, arg):
    """Counts the number of occurances of a substring"""
    return value.count(arg)


@register.simple_tag
def settings_value(name):
    return getattr(settings, name, "")
