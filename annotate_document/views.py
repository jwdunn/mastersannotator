import json
from itertools import chain
from random import sample, randint
from re import sub, match
import logging

from django.db.models import IntegerField, Sum, Case, Count, When, F, Q
from django.conf import settings
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import render, HttpResponse, redirect, get_object_or_404, Http404
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.views.generic.list import ListView

from .decorators import admin_approval_required
from .models import DocumentSegment, Mention, Entity, Profile, Link, DocumentAnnotation, OptOut, SuggestedEdit, \
    VerifyLanguage
from .forms import SignUpForm

logger = logging.getLogger(__name__)


def find_html(text, term, offset=0):
    if len(term) < 1:
        return 0, 0
    start_pos = text.find(term[0], offset)
    while start_pos != -1:
        cursor = 1
        term_match = text[start_pos]
        html_offset = 0
        while cursor < len(term) and start_pos + cursor + html_offset < len(text) and \
                text[start_pos + cursor + html_offset] == term[cursor]:
            term_match += text[start_pos + cursor + html_offset]
            cursor += 1

            if start_pos + cursor + html_offset < len(text) and text[start_pos + html_offset + cursor] == "<":
                html_match = match(r'(</?.*?>)+', text[start_pos + html_offset + cursor:])
                if html_match:
                    html_offset += len(html_match.group(0))
        if term_match == term:
            return start_pos, start_pos + html_offset + cursor
        else:
            start_pos = text.find(term[0], start_pos + cursor + html_offset)
    return -1, -1


def highlight_mention_occurrence(request, mention, mention_id, text):
    name = mention.get('mention__text')
    occurrence = mention.get('mention__occurrence')
    start_html = f'<mark id="prev-{mention_id}" class="green">'
    end_html = '</mark>'
    offset = 0
    count = 0
    start_pos = 0
    while count < occurrence and start_pos != -1:
        start_pos, end_pos = find_html(text, name, offset)
        if start_pos != -1:
            count += 1
        else:
            logger.error(f"Failed to highlight: {name} in {text}")
            if request:
                messages.error(request,
                               f"Oops, Something has gone wrong and we failed to highlight <b>{name}</b>. But  "
                               f"don't worry <em>it has still been saved so you can carry on annotating</em>.",
                               extra_tags='safe')
            return text
        offset = end_pos

    return text[:start_pos] + start_html + text[start_pos:end_pos] + end_html + text[end_pos:]


def highlight_mention(mention, mention_id, text_list):
    pos = mention["mention__start_position"]
    cursor = 0
    index = 0
    start_html = f'<mark id="prev-{mention_id}" class="green">'
    end_html = '</mark>'
    line_offset = len(text_list[index]) - text_list[index].count("&gt") - 2 * text_list[index].count("&lt")
    while pos > cursor + line_offset and index < len(text_list) - 1:
        index += 1
        cursor += len(text_list[index - 1]) - text_list[index].count("&gt") - text_list[index].count("&lt")
        line_offset = len(text_list[index]) - text_list[index].count("&gt") - text_list[index].count("&lt")

    # Calculate start offset in segment
    new_pos, new_end_pos = find_html(text_list[index], mention['mention__text'].split(' ')[0],
                                     max(0, pos - cursor - len(mention['mention__text'])))
    start_index = index
    end_pos = cursor + new_pos + len(mention['mention__text'])
    end_counter = len(text_list[index]) - new_pos
    line_offset = len(text_list[index]) - 2 * text_list[index].count("&gt") - 2 * text_list[index].count("&lt")
    while end_pos > cursor + line_offset and index < len(text_list) - 1:
        cursor += line_offset
        end_counter += len(text_list[index])
        index += 1
    # Update end_pos
    end_term = mention['mention__text'].split(' ')[-1]
    _, no_html_end = find_html(text_list[index], end_term)
    end_local = max(end_pos - cursor, no_html_end)

    end_index = index
    # Add end html
    text_list[end_index] = text_list[end_index][:end_local] + end_html + text_list[end_index][end_local:]
    # Add start html
    text_list[start_index] = text_list[start_index][:new_pos] + start_html + text_list[start_index][new_pos:]
    return text_list


def split_document(size, full_document, request=None, mentions=[], annotated_lines='', use_occurrences=True):
    num_line = 0
    end_html = '<br>'
    new_text = ''
    cursor = 0
    full_document = full_document.replace('\n', '')

    # Split Text into lines
    split_text = []
    while cursor + size < len(full_document):
        break_point = full_document.rfind(' ', cursor, min(cursor + size, len(full_document)))
        if break_point == -1:
            break_point = len(full_document)
        split_text.append(full_document[cursor: break_point])
        cursor = break_point
        num_line += 1
    split_text.append(full_document[cursor: len(full_document)])
    num_line += 1

    # Add highlights
    if not use_occurrences:
        for i in range(len(mentions)):
            split_text = highlight_mention(mentions[i], i, split_text)

    # Add checkboxes
    new_text = ''
    num_line = 0
    for line in split_text:
        new_text += line + \
                    f"<input type='checkbox' class='annotate_checkbox float-right mr-2' id='line-{num_line}' value=1 onclick='app.check_annotated()'" \
                    f"{'checked' if annotated_lines and num_line < len(annotated_lines) and annotated_lines[num_line] == '1' else ''}>" \
                    + end_html
        num_line += 1
    # Add highlights
    if use_occurrences:
        for i in range(len(mentions)):
            new_text = highlight_mention_occurrence(request, mentions[i], i, new_text)
    return new_text, num_line


@login_required
@admin_approval_required
def index(request):
    documents = DocumentAnnotation.objects.filter(profile=request.user.profile).order_by('seq')
    # if no documents assigned
    if not documents.first() or request.user.profile.refresh:
        assign_all_documents(request)
        documents = DocumentAnnotation.objects.filter(profile=request.user.profile).order_by('seq')
    doc = documents.first()
    if not doc:
        return redirect("annotate_document:no-docs")
    return document(request, doc.document.id)


@login_required
@admin_approval_required
def document(request, document_id):
    if not DocumentAnnotation.objects.all().filter(profile=request.user.profile,
                                                   document__id=document_id).exists():
        raise Http404

    document = get_object_or_404(DocumentSegment, pk=document_id)
    ann_documents = DocumentAnnotation.objects.all().filter(profile=request.user.profile).order_by('seq',
                                                                                                   'document__id')
    ann_document = ann_documents.get(document__id=document_id)
    mentions = Link.objects.all().filter(created_by=request.user.profile, mention__document=document)
    mention_list = list(
        mentions.values('mention__text', 'entity__name', 'mention__start_position', 'mention__occurrence'))
    mention_list.sort(key=lambda x: x["mention__start_position"], reverse=True)
    doc_index = [d.get('document__id') for d in ann_documents.values('document__id')].index(document_id) + 1
    document_lines, num_lines = split_document(settings.MAX_LINE_LEN, document.body, request=request,
                                               mentions=mention_list,
                                               annotated_lines=ann_document.annotated_lines)
    if ann_document.annotated_lines == '' or len(ann_document.annotated_lines) < num_lines:
        ann_document.annotated_lines = '0' * num_lines
        ann_document.save()
    id_list = [d.document.id for d in ann_documents]
    current = id_list.index(document_id)
    prev_doc = id_list[current - 1] if current > 0 else None
    next_doc = id_list[current + 1] if current < (len(id_list) - 1) else None

    annotated = 'true' if ann_document.annotated_lines.count('0') == 0 else 'false'
    context = {'document': document, "prev_mentions": mentions,
               "json_mentions": json.dumps(mention_list),
               "prev_doc": prev_doc, "next_doc": next_doc, 'document_lines': document_lines,
               'num_lines': num_lines, "annotated": annotated, "doc_index": doc_index,
               "num_docs": ann_documents.count()}
    return render(request, 'annotate_document/index.html', context)


@login_required
@staff_member_required
def view_page(request, profile_id, document_id):
    if not DocumentAnnotation.objects.all().filter(profile__id=profile_id,
                                                   document__id=document_id).exists():
        raise Http404
    user_profile = Profile.objects.get(pk=profile_id)
    document = get_object_or_404(DocumentSegment, pk=document_id)
    ann_documents = DocumentAnnotation.objects.all().filter(profile=user_profile).order_by('seq', 'document__id')
    ann_document = ann_documents.get(document__id=document_id)
    mentions = Link.objects.all().filter(created_by=user_profile, mention__document=document)
    mention_list = list(
        mentions.values('mention__text', 'entity__name', 'mention__start_position', 'mention__occurrence'))
    mention_list.sort(key=lambda x: x["mention__start_position"], reverse=True)
    doc_index = [d.get('document__id') for d in ann_documents.values('document__id')].index(document_id) + 1
    document_lines, num_lines = split_document(settings.MAX_LINE_LEN, document.body, request=request,
                                               mentions=mention_list,
                                               annotated_lines=ann_document.annotated_lines)
    if ann_document.annotated_lines == '' or len(ann_document.annotated_lines) < num_lines:
        ann_document.annotated_lines = '0' * num_lines
        ann_document.save()
    id_list = [d.document.id for d in ann_documents]
    current = id_list.index(document_id)
    prev_doc = id_list[current - 1] if current > 0 else None
    next_doc = id_list[current + 1] if current < (len(id_list) - 1) else None

    annotated = 'true' if ann_document.annotated_lines.count('0') == 0 else 'false'
    context = {'document': document, "prev_mentions": mentions,
               "json_mentions": json.dumps(mention_list),
               "prev_doc": prev_doc, "next_doc": next_doc, 'document_lines': document_lines,
               'num_lines': num_lines, "annotated": annotated, "doc_index": doc_index,
               "num_docs": ann_documents.count()}
    return render(request, 'annotate_document/index.html', context)


def link(request):
    # Get entity
    entity_name = request.POST.get("entity_radio")
    if entity_name == "-1":
        # Create new entity
        dob = request.POST.get("new_entity_dob")
        dod = request.POST.get("new_entity_dod")
        new_entity = Entity(name=request.POST.get("new_entity_name"),
                            date_of_birth=dob if dob != '' else None,
                            date_of_death=dod if dod != '' else None,
                            description=request.POST.get("new_entity_desc"))
        new_entity.save()
        entity_name = new_entity.id

    # Get user profile
    user_profile = request.user.profile
    user_profile.last_activity = now()

    # Get Document Segment
    document = request.POST.get("document")
    if document:
        document_segment = DocumentSegment.objects.get(pk=document)

    # Get Mention
    mentions = request.POST.get("mentions")
    if mentions:
        mentions_dict = json.loads(mentions)
        for mention_dict in mentions_dict:
            text = mention_dict.get("text").strip()
            start_pos = int(mention_dict.get("start"))
            occurrence = int(mention_dict.get("occurrence"))

            if len(text) == 0:
                messages.warning(request, f"{mention_dict.get('text')} is not a valid mention")
                continue

            # Check if mention should be processed
            if request.POST.get(f"{text}{start_pos}") == "true":
                text = sub(r'\s+', ' ', text)
                context = request.POST.get("context")
                context = sub(r'\s+', ' ', context) if context else ''
                mention = Mention.objects.filter(text=text, start_position=start_pos, document=document_segment,
                                                 occurrence=occurrence)
                # If mention doesn't exist create new mention.
                if not mention:
                    mention = Mention(text=text, start_position=start_pos,
                                      document=document_segment, created_by=request.user,
                                      context=context, occurrence=occurrence)
                    mention.save()
                else:
                    mention = mention.first()

                # Get entity
                entity = Entity.objects.get(pk=int(entity_name))

                # Create link
                if not Link.objects.filter(mention=mention, entity=entity, created_by=user_profile):
                    new_link = Link(mention=mention, entity=entity, created_by=user_profile)
                    new_link.save()

                # Update user profile
                user_profile.num_annotated += 1

                # Handle suggested edits
                suggest_name = None
                suggest_desc = None
                if request.POST.get('check_name'):
                    suggest_name = request.POST.get('edit_entity_name')
                if request.POST.get('check_desc'):
                    suggest_desc = request.POST.get('edit_entity_desc')
                if suggest_name or suggest_desc:
                    SuggestedEdit(entity=entity, name=suggest_name, description=suggest_desc).save()

        # Save user profile
        user_profile.save()

    return redirect("annotate_document:document", document_id=int(request.POST.get('document')))


@login_required
@admin_approval_required
def tutorial(request):
    return render(request, 'annotate_document/tutorial.html')


def search_entities(request):
    if request.method == "POST":
        # Search for entity
        search_term = request.POST["search_entity"]
        try:
            # Fetch candidate entities with similar mentions to search term
            l_candidates = Link.objects.all().filter(mention__text__icontains=search_term).values('entity').distinct()
            candidate_ids = [l.get('entity') for l in l_candidates]
            # Select candidate entities with similar names or mentions to search terms
            entity_candidates = Entity.objects.filter(Q(name__icontains=search_term) | Q(id__in=candidate_ids))
        except Exception as e:
            return JsonResponse({'error_message': e})
        entity_candidate_list = list(entity_candidates.values('id', 'name', 'description'))
        # Extract aliases for entities
        for i in range(len(entity_candidate_list)):
            id = int(entity_candidate_list[i]["id"])
            alias_list = list(Mention.objects.all().filter(link__entity__id=id).values('text').distinct())
            entity_candidate_list[i]["alias"] = ", ".join([m.get('text') for m in alias_list])
        return JsonResponse(entity_candidate_list, safe=False)


def autocomplete_model(request):
    q = request.GET.get('term', '').capitalize()
    search_qs = Entity.objects.filter(name__icontains=q)
    results = []
    for r in search_qs:
        results.append(r.name)
    data = json.dumps(results)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


@login_required
@admin_approval_required
def information(request):
    assign_all_documents(request)

    return render(request, 'annotate_document/information.html')


@login_required
def review(request):
    receive_res = request.user.profile.receive_results
    # Fetch document
    assigned_documents = DocumentAnnotation.objects.filter(profile=request.user.profile)
    assigned_ids = [d['document__id'] for d in
                    assigned_documents.values('document__id')]
    rejected_docs = OptOut.objects.filter(profile=request.user.profile)
    rejected_ids = [d['document__id'] for d in
                    rejected_docs.values('document__id')]

    incomplete_documents = DocumentAnnotation.objects.all().filter(profile=request.user.profile,
                                                                   annotated_lines__contains='0')
    to_be_annotated = DocumentSegment.objects.filter(num_annotations__lte=3) \
        .exclude(id__in=assigned_ids + rejected_ids).count()
    context = {'receive_res': receive_res, 'incomplete_documents': incomplete_documents,
               'annotation_complete': to_be_annotated == 0}
    return render(request, 'annotate_document/review.html', context)


@login_required
def help(request):
    return render(request, 'annotate_document/help.html')


@login_required
def instructions(request):
    return render(request, 'annotate_document/instructions.html')


def no_docs(request):
    return render(request, 'annotate_document/nopages.html')


@login_required
def complete(request):
    profile = request.user.profile
    num_documents = DocumentAnnotation.objects.filter(completed=True, profile=profile).count()
    context = {"words": profile.num_words, "documents": num_documents, "entities": profile.num_annotated}
    return render(request, 'annotate_document/complete.html', context=context)


def send_completion_email(request):
    # Send email notifying user completion
    username = request.GET['username']
    num_words = request.GET['num_words']
    num_documents = request.GET['num_documents']
    num_annotated = request.GET['num_annotated']
    subject = "Annotation Completed"
    message = f"{username} has completed the annotation process.\nAnnotated Words:\t{num_words}" \
              f"\nDocuments Annotated:\t{num_documents}\nEntities Linked:\t{num_annotated}"
    try:
        send_mail(subject, message, settings.EMAIL_ADDR, [settings.EMAIL_ADDR], fail_silently=False)
    except BaseException as e:
        logger.error(e)
        return JsonResponse({"status": 500, "error": f'An error has occurred: {e}'})
    return JsonResponse({"status": "200"})


def assign_all_documents(request, user=None):
    if not user:
        user = request.user

    # Assign documents
    user_profile = user.profile
    num_words = user_profile.num_words
    if num_words < settings.WORDS_TO_ANNOTATE:
        assigned_documents = DocumentAnnotation.objects.filter(profile=user.profile)
        num_words = sum([d["document__words"] for d in assigned_documents.values("document__words")])
        rejected_docs = OptOut.objects.filter(profile=request.user.profile)
        seq_num = assigned_documents.order_by("-seq").first()
        seq_num = seq_num.seq if seq_num else 0
        assigned_ids = [d['document__id'] for d in
                        assigned_documents.values('document__id')]
        rejected_ids = [d['document__id'] for d in
                        rejected_docs.values('document__id')]

        # Assign documents. First assign documents assigned to less than three assignments.
        documents_1 = DocumentSegment.objects.filter(num_assignments__lt=3).exclude(
            id__in=assigned_ids + rejected_ids).order_by("-num_annotations", "num_assignments", "id")[:500]
        if len(documents_1) < 500:
            # If all documents have been assigned at least 3 times then
            # assign documents with the least number of assigned.
            documents_2 = DocumentSegment.objects.filter(num_assignments__gte=3, num_annotations__lt=3) \
                              .exclude(id__in=assigned_ids + rejected_ids) \
                              .order_by("-num_annotations", "num_assignments", "id")[:500]
            documents = list(chain(documents_1, documents_2))  # Combine query sets such that d1 is before d2
        else:
            documents = documents_1

        # Continue assigning until over some maximum
        for new_doc in documents:
            if num_words > settings.WORDS_TO_ANNOTATE:
                break
            num_words += new_doc.words
            if new_doc.num_lines < 1:
                split_text, num_lines = split_document(settings.MAX_LINE_LEN, new_doc.body, request=request)
                new_doc.num_lines = num_lines
            seq_num += 1
            new_annotation = DocumentAnnotation(document=new_doc, profile=user_profile,
                                                annotated_lines='0' * new_doc.num_lines,
                                                seq=seq_num)

            new_annotation.save()
            new_doc.num_assignments += 1
            new_doc.save()

        user_profile.num_words = num_words
        user_profile.refresh = False
        user_profile.save()


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            print(list(request.POST.items()))
            q1 = request.POST.get('question1')
            a1 = request.POST.get('answer1')
            q2 = request.POST.get('question2')
            a2 = request.POST.get('answer2')
            if q1 != a1 or q2 != a2:
                print(f"Q1: {q1}, A1: {a1}\nQ2: {q2}, A2: {a2}")
                messages.add_message(request, messages.WARNING,
                                     'Failed language verificaion: please note that particiants must be able to read isiZulu.')
                return redirect('annotate_document:signup')
            user = form.save()
            user.refresh_from_db()
            user.profile.consent = form.cleaned_data.get('consent') is None
            user.profile.terms = form.cleaned_data.get('consentTerms')
            user.profile.phone = form.cleaned_data.get("phone_number")
            if request.POST.get('lang_disclaimer'):
                user.profile.language = request.POST.get('lang_disclaimer') == "True"
            user.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)

            return redirect('annotate_document:info')
        else:
            print(form.errors)
    else:
        form = SignUpForm()

    # Add Language Verification
    questions = VerifyLanguage.objects.all()
    groups = sample([q['group'] for q in questions.values('group').distinct()], 2)
    question_1 = questions.filter(group=groups[0])
    answer_1 = question_1[randint(0, question_1.count() - 1)]

    question_2 = questions.filter(group=groups[1])
    answer_2 = question_2[randint(0, question_2.count() - 1)]

    remaining = DocumentSegment.objects.filter(num_annotations__lt=3).count()
    context = {'form': form, 'documents_remaining': remaining > 0, 'q1': question_1, 'q2': question_2,
               'a1': answer_1, 'a2': answer_2}
    return render(request, 'annotate_document/signup.html', context)


@login_required
def update_receive_res(request):
    if request.method == 'POST':
        response = request.POST.get('receiveRes')
        if response:
            request.user.profile.receive_results = response == "Yes"
            if response == "Yes":
                request.user.profile.email = request.POST.get('email')
            else:
                request.user.profile.email = ''
            request.user.profile.save()
        return JsonResponse({"choice": response})


@login_required
def delete_link(request):
    if request.method == 'POST':
        text = request.POST.get('text')
        pos = request.POST.get('pos')
        entity = request.POST.get('entity')
        try:
            link = Link.objects.filter(created_by=request.user.profile, mention__text=text,
                                    mention__start_position=pos, entity__name=entity)
        except Link.DoesNotExist:
            link = None

        if link:
            # Delete link
            link.delete()
            return JsonResponse({"status": 200})

        return JsonResponse({"status": 400})


def get_offsets(text, tokens):
    offsets = []
    cursor = 0
    for token in tokens:
        pos = text.find(token, cursor)
        offsets.append(pos)
        cursor = pos + 1
    return offsets


def token_index(pos, offset):
    p = 0
    while p < (len(offset) - 1) and pos >= offset[p + 1]:
        p += 1
    #print(pos, p, offset[p], offset[p+1])
    return p

def find_occurance(text, term, n):
    start = text.find(term)
    while start >= 0 and n > 1:
        start = text.find(term, start+len(term))
        n -= 1
    return start


def get_tags(text, tokens, links):
    offsets = get_offsets(text, tokens)
    iob_tags = [tuple('O')] * len(tokens)
    el_tags = [tuple('')] * len(tokens)

    for link in links:
        mention = link.mention
        start_pos = find_occurance(text, mention.text, mention.occurrence)
        iob_index = token_index(start_pos, offsets)
        if mention.text.strip().find(' ') == -1:
            if iob_tags[iob_index] == tuple('O'):
                # name is not nested
                iob_tags[iob_index] = tuple("I")
                el_tags[iob_index] = (link.entity.id,)
            else:
                # name is nested
                iob_tags[iob_index] += tuple("I")
                el_tags[iob_index] += (link.entity.id,)

        else:
            if iob_tags[iob_index] == tuple('O'):
                iob_tags[iob_index] = tuple("B")
                el_tags[iob_index] = (link.entity.id,)
            else:
                iob_tags[iob_index] += tuple("B")
                el_tags[iob_index] += (link.entity.id,)
            for i in range(1, mention.text.count(' ') + 1):
                if iob_tags[iob_index + i] == tuple("O"):
                    iob_tags[iob_index + i] = tuple("I")
                    el_tags[iob_index + i] = (link.entity.id,)
                else:
                    # name is nested
                    iob_tags[iob_index] += tuple("I")
                    el_tags[iob_index] += (link.entity.id,)
    return iob_tags, el_tags


def aggregate_annotations(el_tags):
    zipped_tags = list(zip(*el_tags))
    res = []
    for tag in zipped_tags:
        tag_res = {}
        max_val = -1
        max_tag = ''
        for entry in tag:
            if entry not in tag_res.keys():
                tag_res[entry] = 1
            else:
                tag_res[entry] += 1

        for key, val in tag_res.items():
            if val > max_val:
                max_val = val
                max_tag = key
        tag_res["max"] = max_tag
        res.append(tag_res)
    return res


def get_annotated(document_id):
    document = DocumentSegment.objects.get(pk=document_id)
    # mentions = Mention.objects.all().filter(created_by=request.user, document=document)
    links = Link.objects.all().filter(mention__document=document)
    # users = links.distinct('created_by').values('created_by')
    users = DocumentAnnotation.objects.filter(document_id=document_id).values('profile_id')
    links = links.order_by('mention__start_position')
    tokens = document.body.split() #document.tokens.split()
    all_ner_tags = []
    all_el_tags = []
    for user in users:
        user_links = links.filter(created_by=user.get('profile_id'))
        # Get NER tags
        ner_tags, el_tags = get_tags(document.body, tokens, user_links)
        all_ner_tags.append(ner_tags)
        all_el_tags.append(el_tags)
    agg_el_tags = aggregate_annotations(all_el_tags)
    agg_ner_tags = aggregate_annotations(all_ner_tags)
    res = {'id': document_id, 'tokens': tokens, "ner_tags": [t.get("max") for t in agg_ner_tags],
           "all_ner_tags": all_ner_tags,
           "el_tags": [t.get("max") for t in agg_el_tags], "all_el_tags": all_el_tags}
    return res


@admin_approval_required
def export(request, document_id):
    if not request.user or not request.user.is_superuser:
        return HttpResponseForbidden()
    return JsonResponse(get_annotated(document_id))


@admin_approval_required
def export_all(request):
    if not request.user or not request.user.is_superuser:
        return HttpResponseForbidden()
    all_res = []
    documents = DocumentSegment.objects.all()
    for document in documents:
        all_res.append(get_annotated(document.id))
    return JsonResponse(all_res, safe=False)


def update_annotated(request):
    if request.method == "POST":
        profile = request.user.profile
        profile.last_activity = now()
        profile.save()

        if request.POST.get('document_id') != None:
            document = DocumentAnnotation.objects.get(profile=profile,
                                                      document__id=int(request.POST.get('document_id')))
            document.annotated_lines = request.POST.get('annotated_lines')

            if document.completed and '0' in document.annotated_lines:
                document.completed = False
                document.document.num_annotations = DocumentAnnotation.objects\
                    .filter(document__id=int(request.POST.get('document_id')), completed=True) \
                    .count() - 1
                document.document.save()
            elif '0' not in document.annotated_lines and not document.completed:
                document.completed = True
                document.document.num_annotations = DocumentAnnotation.objects \
                    .filter(document__id=int(request.POST.get('document_id')), completed=True) \
                    .count() + 1
                document.document.save()
            document.save()
    return JsonResponse({'status': 200})


def assign_document(request):
    user_profile = request.user.profile
    num_words = user_profile.num_words
    # Fetch document
    assigned_docs = DocumentAnnotation.objects.filter(profile=request.user.profile)
    rejected_docs = OptOut.objects.filter(profile=request.user.profile)
    seq_num = assigned_docs.order_by("-seq").first()
    seq_num = seq_num.seq if seq_num else 0
    assigned_ids = [d['document__id'] for d in
                    assigned_docs.values('document__id')]
    rejected_ids = [d['document__id'] for d in
                    rejected_docs.values('document__id')]
    # Assign documents. First assign documents assigned to less than three assignments.
    new_doc = DocumentSegment.objects.filter(num_assignments__lt=3) \
        .exclude(id__in=assigned_ids + rejected_ids) \
        .order_by("-num_assignments", "-num_annotations") \
        .first()

    if not new_doc:
        # If all documents have been assigned at least 3 times then
        # assign documents with the least number of assigned.
        new_doc = DocumentSegment.objects.filter(num_annotations__lt=3) \
            .exclude(id__in=assigned_ids + rejected_ids) \
            .order_by("-num_annotations", "num_assignments").first()

    # Assign new document
    if new_doc:
        num_words += new_doc.words
        if new_doc.num_lines < 1:
            split_text, num_lines = split_document(settings.MAX_LINE_LEN, new_doc.body, request=request)
            new_doc.num_lines = num_lines
        seq_num += 1
        new_annotation = DocumentAnnotation(document=new_doc, profile=user_profile,
                                            annotated_lines='0' * new_doc.num_lines,
                                            seq=seq_num)
        new_annotation.save()
        new_doc.num_assignments += 1
        new_doc.save()

        user_profile.num_words = num_words
        user_profile.save()

        return new_annotation
    return None


@login_required
def add_document(request):
    new_doc = assign_document(request).document
    if new_doc is None:
        # If no new_documents redirect to
        messages.warning(request, "Hmm.. Looks like there are no more documents for you to annotate! "
                                  "Thank you for your help!")
        return redirect("annotate_document:document-list")

    # redirect to page for new document
    return redirect("annotate_document:document", document_id=new_doc.id)


def update_tutorial(request):
    if not request.user.profile.tutorial:
        request.user.profile.tutorial = True
        request.user.profile.save()
    return redirect("annotate_document:index")


def update_instructions(request):
    if not request.user.profile.tutorial_video:
        request.user.profile.tutorial_video = True
        request.user.profile.save()
    return redirect("annotate_document:tutorial")


@admin_approval_required
@staff_member_required
def get_payments(request):
    response = {}
    for profile in Profile.objects.all():
        response[profile.id] = {'user': profile.id, "links": 0, 'phone': profile.phone, "entries": []}

        documents = list(DocumentAnnotation.objects.filter(profile=profile, completed=True).values("document"))
        counter = 0
        for doc_id in documents:
            # Collect all mentions
            mentions = Mention.objects.filter(document=doc_id['document'])
            # For each mention get all links
            for mention in mentions:
                #print(mention.text)
                links = Link.objects.filter(mention=mention)
                user_link = links.filter(created_by=profile)
                if len(user_link) == 1:
                    user_link = user_link[0]
                    # Count majority
                    link_res = {}
                    for link in links:
                        #print(f"\t{link.created_by}: {link.entity}")
                        if link.entity in link_res.keys():
                            link_res[link.entity] += 1
                        else:
                            link_res[link.entity] = 1
                    max_num_links = max(link_res.values())
                    max_entities = [f"{entity.name} ({entity.id})" for entity in link_res.keys() if link_res[entity] == max_num_links]
                    if link_res[user_link.entity] == max_num_links and max_num_links > 1:
                        response[profile.id]["links"] += 1
                    response[profile.id]["entries"].append(
                        {'user_entity': f"{user_link.entity.name} ({user_link.entity.id})", 'max_entity': max_entities,
                         'max_links': max_num_links, 'total_links': len(links)})
        counter += 1
        if counter % 100 == 0:
            print(f"Processed {counter}/{len(Profile.objects.all())} documents")
    # Document level info
    doc_res = DocumentAnnotation.objects.values('profile') \
        .annotate(assigned_document=Count("document__id"),
                  num_words=Sum("document__words"),
                  unannotated=Count("document__id", filter=Q(annotated_lines__contains='0')),
                  annotated=Sum(
                      Case(When(annotated_lines__contains='0', then=0),
                           default=1, output_field=IntegerField())),
                  annotated_words=Sum(
                      Case(When(annotated_lines__contains='0', then=0),
                           default=F("document__num_words")))
                  ).order_by('profile')

    # Assign document stats to the correct profile in response
    for res in doc_res:
        profile_id = res.get('profile')
        if profile_id:
            for k, v in res.items():
                #print(k, v)
                if k != 'profile':
                    response[profile_id][k] = v

    return JsonResponse(response)


@login_required
def optout(request):
    doc_id = int(request.POST.get('segment_id'))
    profile = request.user.profile
    doc = DocumentSegment.objects.get(pk=doc_id)

    # Delete document annotation object
    ann_documents = DocumentAnnotation.objects.all().filter(profile=profile).order_by('seq', 'document__id')
    ann_document = ann_documents.get(document__id=doc_id)
    seq = ann_document.seq
    completed = '0' not in ann_document.annotated_lines
    ann_document.delete()

    # Delete links
    Link.objects.filter(created_by=profile, mention__document__id=doc_id).delete()

    # Update user Profile
    profile.num_words -= doc.words
    num_links = Link.objects.filter(created_by=profile, mention__document__id=doc_id)
    if num_links:
        profile.num_annotated -= num_links.count()
    profile.save()

    # Update DocumentSegment
    doc.num_assignments -= 1
    if completed:
        doc.num_annotations -= 1
    doc.save()

    # Create new Opt-out
    OptOut(profile=profile, document=doc).save()

    # Add new document
    new_doc = assign_document(request)
    if not new_doc:
        # Set to next document
        next_doc = ann_documents.filter(seq__gt=seq).first()
        if next_doc:
            print(f"Got to Next: Current seq: {seq}, next: {next_doc.seq}")
            return redirect("annotate_document:document", document_id=next_doc.document.id)
        else:
            # if last document set to second last
            next_doc = ann_documents.order_by("-seq").first()
            if next_doc:
                print(f"Got to Last: Current seq: {seq}, next: {next_doc.seq}")
                return redirect("annotate_document:document", document_id=next_doc.document.id)
        return redirect("annotate_document:document-list")
    new_doc.seq = seq
    new_doc.save()

    return redirect("annotate_document:document", document_id=new_doc.document.id)


def user_page(request):
    if not request.user.profile.tutorial_video:
        return redirect('annotate_document:instructions')
    elif not request.user.profile.tutorial:
        return redirect('annotate_document:tutorial')
    return redirect('annotate_document:document-list')


@login_required
def show_video(request):
    return render(request, 'annotate_document/video.html')


def invitation(request):
    return render(request, 'annotate_document/invitation.html')


@method_decorator(admin_approval_required, name='dispatch')
class DocumentListView(LoginRequiredMixin, ListView):
    model = DocumentAnnotation
    paginate_by = 15

    def get_queryset(self):
        new_context = self.model.objects.filter(profile=self.request.user.profile).order_by('seq', 'document__id')
        return new_context

    def get_context_data(self, **kwargs):
        assign_all_documents(self.request)
        context = super().get_context_data(**kwargs)
        annotated = Mention.objects.filter(created_by=self.request.user).values('document')
        context['annotated'] = [m.get('document') for m in annotated]
        return context
