from django.contrib import admin

from .models import DocumentSegment, DocumentAnnotation, Entity, Link, Mention, Profile, SiteApproval, OptOut, \
    SuggestedEdit, VerifyLanguage


class DocumentAnnotationAdmin(admin.ModelAdmin):
    readonly_fields = ('assigned_at',)


class ProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('last_activity',)


admin.site.register(DocumentSegment)
admin.site.register(DocumentAnnotation, DocumentAnnotationAdmin)
admin.site.register(Entity)
admin.site.register(Link)
admin.site.register(Mention)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(SiteApproval)
admin.site.register(OptOut)
admin.site.register(SuggestedEdit)
admin.site.register(VerifyLanguage)
