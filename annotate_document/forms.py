from django import forms
from django.core.validators import RegexValidator
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class SignUpForm(UserCreationForm):
    phone_error = 'Phone number should have the following format: +27121231234.' \

    phone_help = 'Include international dialing codes e.g the South African number ' \
                 '(dialing code +27) 012 123 1234 becomes +27121231234'

    # Phone number validator
    phone_regex = RegexValidator(regex=r'^\+\d{11}$', message=phone_error)

    phone_number = forms.CharField(required=True, max_length=20, validators=[phone_regex], help_text=phone_help,
                                   widget=forms.TextInput(attrs={'type': 'tel',
                                                                 'class': 'form-control',
                                                                 'placeholder': '+27121231234',
                                                                 }))

    consent = forms.BooleanField(required=True, initial=False)
    consentTerms = forms.BooleanField(required=True, initial=False, label="Terms and Privacy")

    def clean_consent(self):
        data = self.cleaned_data['consent']
        if not data:
            raise ValidationError("You must give consent for your participation in the survey")

    class Meta:
        model = User
        fields = ('username', 'phone_number', 'password1', 'password2', 'consent')
        widgets = {
            'username': forms.TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'password': forms.PasswordInput(
                attrs={
                    'class': 'form-control'
                }
            ),
            'password2': forms.PasswordInput(
                attrs={
                    'class': 'form-control'
                }
            )
        }
