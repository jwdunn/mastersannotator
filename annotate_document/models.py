from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.dispatch import receiver
from django.db import models
from django.db.models.signals import post_save


class DocumentSegment(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    body = models.TextField()
    tokens = models.TextField()
    part = models.IntegerField(default=1)
    total_parts = models.IntegerField(default=1)
    num_annotations = models.IntegerField(default=0)
    num_assignments = models.IntegerField(default=0)
    num_lines = models.IntegerField(default=-1)
    words = models.IntegerField(default=-1)


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    num_annotated = models.IntegerField(default=0)
    num_words = models.IntegerField(default=0)
    consent = models.BooleanField(default=False)
    terms = models.BooleanField(default=False)
    language = models.BooleanField(default=False)
    receive_results = models.BooleanField(default=False)
    admin_approval = models.BooleanField(default=False)
    phone = models.CharField(max_length=20, default='+27121231234')
    tutorial = models.BooleanField(default=False)
    tutorial_video = models.BooleanField(default=False)
    email = models.EmailField(null=True, default='')
    last_activity = models.DateTimeField(auto_now_add=True)
    refresh = models.BooleanField(default=False)


class DocumentAnnotation(models.Model):
    id = models.AutoField(primary_key=True)
    seq = models.IntegerField(default=0);
    document = models.ForeignKey(DocumentSegment, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    annotated_lines = models.CharField(max_length=100, default='')
    completed = models.BooleanField(default=False)
    assigned_at = models.DateTimeField(auto_now_add=True)


class OptOut(models.Model):
    id = models.AutoField(primary_key=True)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    document = models.ForeignKey(DocumentSegment, on_delete=models.CASCADE)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Mention(models.Model):
    class Meta:
        models.UniqueConstraint(fields=['text', 'start_position', 'document'], name='uniqueMention')

    id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=250, null=False, blank=False)
    context = models.TextField(null=True, default='')
    document = models.ForeignKey(DocumentSegment, on_delete=models.CASCADE, null=False, blank=False)
    start_position = models.IntegerField(default=0, null=False)
    occurrence = models.IntegerField(default=1, null=False)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class Entity(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=250, null=False)
    date_of_birth = models.DateField(null=True)
    date_of_death = models.DateField(null=True)
    description = models.TextField(null=True)
    fhya_id = models.CharField(null=True, max_length=20)


class Link(models.Model):
    id = models.AutoField(primary_key=True)
    mention = models.ForeignKey(Mention, on_delete=models.CASCADE)
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    created_by = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class SiteApproval(models.Model):
    id = models.AutoField(primary_key=True)
    site = models.OneToOneField(Site, on_delete=models.CASCADE)
    require_approval = models.BooleanField(default=False)


class SuggestedEdit(models.Model):
    id = models.AutoField(primary_key=True)
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=10, null=False, blank=False, default="PENDING")
    completed = models.BooleanField(default=False)


class VerifyLanguage(models.Model):
    id = models.AutoField(primary_key=True)
    group = models.IntegerField(null=False, default=0)
    english = models.CharField(max_length=100, null=False, blank=False)
    zulu = models.CharField(max_length=100, null=False, blank=False)