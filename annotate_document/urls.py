from django.urls import path

from . import views

app_name = 'annotate_document'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:document_id>', views.document, name='document'),
    path('view/<int:profile_id>/<int:document_id>', views.view_page, name='view_page'),
    path('search', views.search_entities, name='search'),
    path('link', views.link, name='link'),
    path('list', views.DocumentListView.as_view(), name='document-list'),
    path('info', views.information, name='info'),
    path('review', views.review, name='review'),
    path('complete', views.complete, name='complete'),
    path('signup', views.signup, name='signup'),
    path('update_res', views.update_receive_res, name='updateReceiveRes'),
    path('export/<int:document_id>', views.export, name="export"),
    path('export/', views.export_all, name="export"),
    path('update_annotated', views.update_annotated, name="updateAnnotated"),
    path('tutorial', views.tutorial, name="tutorial"),
    path('help', views.help, name="help"),
    path('instructions', views.instructions, name="instructions"),
    path('delete_link', views.delete_link, name="deleteLink"),
    path('add_doc', views.add_document, name="addDocument"),
    path('update_tut', views.update_tutorial, name="updateTutorial"),
    path('update_instr', views.update_instructions, name="updateInstructions"),
    path('payments', views.get_payments, name="payments"),
    path('user_page', views.user_page, name="userPage"),
    path('video', views.show_video, name="video"),
    path('invitation', views.invitation, name="invitation"),
    path('send-completion', views.send_completion_email, name="sendCompletion"),
    path('opt-out', views.optout, name="opt-out"),
    path('empty', views.no_docs, name="no-docs")
]
