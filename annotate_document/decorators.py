from functools import wraps

from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import PermissionDenied

from .models import SiteApproval


def admin_approval_required(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        current_site = get_current_site(request)
        site_approval = SiteApproval.objects.get(site=current_site)
        if site_approval.require_approval:
            if not request.user.is_superuser and not request.user.profile.admin_approval:
                raise PermissionDenied("Your user has not been approved. Please contact the site admin")
        return function(request, *args, **kwargs)

    return wrap
