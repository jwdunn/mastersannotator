from django.apps import AppConfig


class AnnotateDocumentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'annotate_document'
