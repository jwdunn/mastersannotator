//
if (!window.x) {
	x = {};
}

// Get selected text
x.Selector = {};
x.Selector.getSelected = function() {
    var t = '';
    if (window.getSelection) {
        t = window.getSelection();
    } else if (document.getSelection) {
        t = document.getSelection();
    } else if (document.selection) {
        t = document.selection.createRange().text;
    }
    return t;
}

var pageX;
var pageY;
var textOffsetX;
var textOffsetY;
var newHighlight = true;

function toggleHighlight(element){
    var id = parseInt(element.id);
    var highlightSection = $(`mark#${id + 1}`);
    if (element.checked){
        // Highlight element
        if (highlightSection.hasClass('none')){
            highlightSection.removeClass('none');
        }
    } else {
        // Remove element highlight
        highlightSection.addClass('none');
    }
}

function showMenu(){
    $("#entityModal").modal('show');
}

// Check/uncheck line completed checkbox
$(".annotate_checkbox").click(function () {
    const annotatedLines = $("#annotatedLines").val();
    const document = $("#docId").val();
    const csrf = $('input[name=csrfmiddlewaretoken]').val();
     $.ajax({
        type: "POST",
        url: '{% url "annotate_document:updateAnnotated" %}',
        data: {
            'annotated_lines': annotatedLines,
            'document_id': document,
            'csrfmiddlewaretoken': csrf,
        },
        success: function (data) {
        $("#last_saved").html("Last Saved: " + new Date().toLocaleString());
        }
    });
});

// Delete link
$("#delete_btn").click(function () {
    const csrf = $('input[name=csrfmiddlewaretoken]').val();
    mention = app.prev_mentions[app.highlighted_mention_index];
     $.ajax({
        type: "POST",
        url: '{% url "annotate_document:deleteLink" %}',
        data: {
            'text': mention.mention__text,
            'entity': mention.entity__name,
            'pos': mention.mention__start_position,
            'csrfmiddlewaretoken': csrf,
        },
        success: function (data) {
            if (data.status == 200){
                var search = new RegExp(`<mark id="prev-${app.highlighted_mention_index}" class="green">.*?</mark>`, "g")
                mark = $(`#prev-${app.highlighted_mention_index}`)
                if (mark.hasClass('green')){
                    mark.removeClass('green');
                    mark.addClass('none');
                }
            }
        }
    });
});

// Show edit buttons when entity selected
$(document).on("click","input[name=entity_radio]", function() {
    // Hide all edit buttons
    $(".editEntity").each(function () {$(this).hide()})
    // Reset edit entity
    $("#entityEdit").collapse("hide");
    if ($("#checkEditName").is(":checked")){
        $("#checkEditName").click()
    }
    if ($("#checkEditDesc").is(":checked")){
        $("#checkEditDesc").click()
    }

    var checked = $("input[name=entity_radio]:checked").val();
    if (checked) {
        $("#entityEditBtn"+checked).show();
    }
});


$(document).ready(function() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    })

    // Hide/show form sections
    checkName = $("#checkEditName")
    editName = $('#editNameForm');
    editName.hide()

    // Show form for suggesting a name edit else hide.
    checkName.on('click', function() {
        if($(this).is(':checked')) {
            editName.show();
            editName.find('input').attr('required', true);
        } else {
            editName.hide();
            editName.find('input').attr('required', false);
        }
    });

    checkDesc = $("#checkEditDesc");
    editDesc = $('#editDescForm');
    editDesc.hide()

    // Show form for suggesting a description edit else hide.
    checkDesc.on('click', function() {
       if($(this).is(':checked')) {
          editDesc.show();
          editDesc.find('input').attr('required', true);
       } else {
          editDesc.hide();
          editDesc.find('input').attr('required', false);
       }
    });

    // Get selected text and show popup menu including the text
    $(document).bind("mouseup", function(e) {
        var selectedText = x.Selector.getSelected();
        var textboxNode = document.getElementById("document_text")
        // Show menu when text is selected
        if(selectedText.toString().trim() != '' && !app.clearHighlight
            && textboxNode.contains(selectedText.getRangeAt(0).commonAncestorContainer)){
                var r = selectedText.getRangeAt(0);
                var selectionRect = r.getBoundingClientRect();
                if (newHighlight){
                    var range = app.word_range(selectedText);
                    app.name = range.toString();
                    app.add_entity_name = app.name;
                    app.newHL = true
                    var textOffset = $("#document_text").offset()
                    $('#hlMention').css({
                            'left': (selectionRect.left - textOffset.left) + 'px',
                            'top' : (pageY - textOffset.top - (1.2*$('#hlMention').height())) + 'px'
                    }).fadeIn(200);
                    newHighlight=false;
            }
        } else if (e.target.tagName == "MARK" && e.target.className == "green"){
            // If a link is clicked show delete link menu
            index = e.target.id.match(/(\d+)/)[0];
            mention = app.prev_mentions[parseInt(index)]
            app.highlighted_mention_index = parseInt(index)
            app.name = mention.entity__name
            app.delete_mention = mention
            var textOffset = $("#document_text").offset()
            $('#rmMention').css({
                'left': (pageX - textOffset.left) + 'px',
                'top' : (pageY - textOffset.top - (1.2*$('#rmMention').height())) + 'px'
            }).fadeIn(200);
         } else {
            $('div.hlMenu').fadeOut(200);
            newHighlight=true;
            app.clearHighlight = false;
        }
    });

    // Close popup menu
    $(document).on("mousedown", function(e){
        pageX = e.pageX;
        pageY = e.pageY;

        setTimeout(function(){
            var selectedText = window.getSelection().toString();
            if(selectedText == ''){
                $('#hlMention').fadeOut(200);
                newHighlight=true;
                app.clearHighlight = false;
            }
        })
    });
});