import json

from django.core.exceptions import ImproperlyConfigured
from django.utils.log import DEFAULT_LOGGING


from annotator.settings.base import *

WSGI_APPLICATION = 'annotator.wsgi.wsgi_production.application'

DEBUG = False
with open(f'{BASE_DIR}/annotator/settings/db_config.json', 'r') as secrets_file:
    db_config = json.load(secrets_file)

def get_secrets(setting, secrets=db_config):
    """Get secret setting or fail with ImproperlyConfigured"""
    try:
        return secrets[setting]
    except KeyError:
        raise ImproperlyConfigured("Set the {} setting".format(setting))


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_secrets("DB_NAME"),
        'USER': get_secrets("DB_USER"),
        'PASSWORD': get_secrets("DB_PASSWORD"),
        'HOST': get_secrets("DB_HOST"),
        'port': get_secrets("DB_PORT")
    }
}

PRODUCTION_ALLOWED_HOSTS = ['54.74.16.211', 'annotator.weblathe.net']
ALLOWED_HOSTS = ALLOWED_HOSTS + PRODUCTION_ALLOWED_HOSTS

# Email settings
EMAIL_HOST_USER = get_secrets("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = get_secrets("EMAIL_HOST_PASSWORD")
EMAIL_HOST = get_secrets("EMAIL_HOST")
EMAIL_ADDR = get_secrets("EMAIL_ADDR")
EMAIL_ADMIN = get_secrets("EMAIL_ADMIN")
SERVER_EMAIL = EMAIL_ADDR 
ADMINS = [('Admin', EMAIL_ADDR)]

DEFAULT_LOGGING['handlers']['console']['filters'] = []
DEFAULT_LOGGING['loggers'][''] = {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True
}
DEFAULT_LOGGING['formatters']['detailed'] = {
            'format': '{levelname} {asctime} {pathname}:{funcName}:{lineno} {message}',
            'style': '{',
}
DEFAULT_LOGGING['handlers']['console']['formatter'] = 'detailed'
