import os

from annotator.settings.base import *


# Local settings
DEBUG = True

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ["DB_NAME"],
        'USER': os.environ["DB_USER"],
        'PASSWORD': os.environ["DB_PASSWORD"],
        'HOST': os.environ["DB_HOST"],
        'port': os.environ["DB_PORT"]
    }
}

# Email settings
EMAIL_HOST_USER = os.environ["EMAIL_ADDR"]
EMAIL_HOST_PASSWORD = os.environ["EMAIL_PASSWORD"]
EMAIL_ADMIN = os.environ.get("EMAIL_ADMIN")
ADMINS = [('Admin', os.environ["EMAIL_ADDR"])]
