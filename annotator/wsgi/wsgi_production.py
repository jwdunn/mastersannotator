import os
import sys
sys.path.append('/opt/bitnami/projects/mastersannotator')
os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/bitnami/projects/mastersannotator/egg_cache")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "annotator.settings.production")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
