# The Annotator
The annotator is an annotation system designed to allow users to select mentions of entities then link them to a record for the entity from a knowledge base. If no record exists, the users can create new entity records. To increase the quality of the labels produced, the final labels can be produced via a majority vote. The tutorial video can be seen [here](https://youtu.be/74LLnY_p-lM) to get an idea of how the system works from a volunteer annotator's perspective.

First a set of documents and possibly an initial set of entities can be loaded into the database. Users then create accounts and can be assigned a number of documents to annotate. If the user completes the documents assigned to them they can request additional documents to annotate. The Annotator required each document to be annotated by three separate volunteers, although this can be configured to require more or less. The final labels are then determined by taking the majority vote between the labels created. Once the documents have been annotated the labels for each document can be exported in the form of a JSON object with IOB and entity id labels for each of the tokens. In the original experiment volunteers were paid based on the number of links where the entity they selected was the correct entity based on a majority vote. The payment details for the system may also be generated as a JSON object including the user's user id, the number of words, documents and links they created and the number of correct links (based on majority agreement) they created.

### Background
This system was originally created to labelled a subsections of the documents from the [500 Year Archive (FHYA)](https://fhya.org/). The documents were written in English and isiZulu. Thus features were added to the signup page to mitagate against users who are not fluent in isiZulu participating.

### Architecture
The Annotator is written primarily in Python using the Django framework and a PostGreSQL database. The JavaScript is mostly written using the Vue.js library with some Ajax queries.

### Future Changes
Since The Annotator was built based on a farily tight time frame it is still a bit rough around the edges. More work it needed to neaten up the JavaScript and move it into a separate file :| There are also places where the code and configurations are too tightly coupled. The addition of unti tests would also be great. I think there is also scope for many new features such as moderating user suggestions, allowing users to fix noise in the text, removing or allowing different forms on annotation such as translation or sentiment analysis.

## Installing The Annotator
**Note** 
This project used a PostgreSQL database. Thus the following instructions assume that a
PostgreSQL database will be used, and that PostgreSQL has already been installed.
However, it is relatively easy to change the database should you choose to do so.


### 1. Clone the Repository
Create a new directory called annotator
```
mkdir annotator
cd annotator
```
Clone the repository found at: https://gitlab.com/jwdunn/mastersannotator
```
git clone https://gitlab.com/jwdunn/mastersannotator.git
```

### 2. Setup virtual environment
This project uses virtenv to create a virtual environment for managing python packages.
This section describes setting up and installing virtenv and the dependencies required for running The Annotator.

#### 1. Install virtualenv
```
/annotator$ pip install virtualenv
```

#### 2. Create New Virtual Environment
```
/annotator$ python3 -m virtualenv venv
```

##### 3. Activate virtual environment
```
/annotator$ source venv/bin/activate
```

### 3. Install Dependencies
```
/annotator$ pip install -r mastersannotator/requirements.txt
```
**Note**  
When trying to install Ubuntu, the *psycopg2* library failed to install with an error like:  
```
error: command 'x86_64-linux-gnu-gcc' failed with exit status 1
```
Installing the following libraries then re-running the command solved the error.
```
/annotator$ sudo apt install python3-dev libpq-dev
```
### 4. Create database
Run the following to create a new user and a new database and set the new user as the owner of the ned database.

```
$ psql -U postgres
$ CREATE USER <user_name> WITH ENCRYPTED PASSWORD '<password>';
$ CREATE DATABASE <db_name>;
$ GRANT ALL PRIVILEGES ON DATABASE <db_name> To <user_name>;
$ exit
```

### 5. Create Environment Variables
The Annotator requires the following variables to be specified:
* DB\_USER=<user_name>
* DB\_NAME=<db_name>
* DB\_PASSWORD=<password>
* DB\_HOST=localhost
* DB\_PORT=5432
* DJANGO\_SETTINGS_MODULE=annotator.settings.local

These can be set either by running export in the shell, e.g. export DB_USER=<user_name>
or by adding the following lines to /annotator/venv/bin/activate:
Each environment variable must be unset in the *deactivate* function. For example:
```
deactivate () {  
    ...
    UNSET DB_USER
}
```
The environment variables are set by adding an export command for each variable to the end of /annotator/venv/bin/activate.
```
export DB_USER=<user_name>
export DB_NAME=<db_name>
export DB_PASSWORD=<password>
export DB_HOST=localhost
export DB_PORT=5432
export DJANGO_SETTINGS\_MODULE=annotator.settings.local
export EMAIL_ADDR="<gmail_email_address>"
export EMAIL_PASSWORD="<password_for_gmail_email_address>"
export EMAIL_ADMIN="<email_addresss>"
```

**Note: Email Settings**  
The system is currently set up to use "smtp.gmail.com" as the email host. However, this
can be changed by editing the EMAIL\_HOST variable in annotator/settings/base.py.  
The notifications due to completions or errors are sent using *EMAIL\_ADDR*.  
*EMAIL\_ADMIN* is used as the help email in the page footers.  

**Note: Production Settings**  
In a production setting use DJANGO\_SETTINGS\_MODULE=annotator.settings.production
Also, in production, the environment variables are stored in a JSON file called annotator/settings/db\_config.json.  
For example:  
```
{
  "DB_NAME": "<user_name>",
  "DB_USER": "<db_name>",
  "DB_PASSWORD": "<password>",
  "DB_HOST": "localhost",
  "DB_PORT": "5432",
  "EMAIL_ADDR": "<gmail_email_address>",
  "EMAIL_PASSWORD": "<password_for_gmail_email_address>"
  "EMAIL_ADMIN" "<email_address"
}
```

### 6. Initialising the DB
To populate the DB with the tables required for The Annotator.
```
mastersannotator$ python manage.py makemigrations
No changes detected
mastersannotator$ python manage.py migrate
```
Then add changes for the annotate\_document app:
```
mastersannotator$ python manage.py makemigrations annotate\_document
Migrations for 'annotate\_document':
  annotate_document/migrations/0001_initial.py
    - Create model DocumentSegment
    - Create model Entity
    - Create model VerifyLanguage
    - Create model SuggestedEdit
    - Create model SiteApproval
    - Create model Profile
    - Create model OptOut
    - Create model Mention
    - Create model Link
    - Create model DocumentAnnotation

mastersannotator$ python manage.py migrate
```

### 7. Start the server
To start the server run
```
mastersannotator$ python manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
January 10, 2022 - 13:28:46
Django version 3.2, using settings 'annotator.settings.local'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```
By following the link to the development server (http://127.0.0.1:8000), you should see The Annotator running.
However, there are currently no users or documents loaded.

### 8. Adding a Super User
Following the link to http://127.0.0.1:8000 shows a login page. However, there are currently
no users in the database. You could create users using the signup method; however, these users
will only annotate documents.  
To create a user with Admin privileges, stop the server, then run and follow the prompts.
```
mastersannotator$ python manage.py createsuperuser
```
Once the superuser has been created, you can restart the server then enter the super user's
username and password into the login page. The next page produces an error due to one of the values 
in the database not being set. To fix this got to the admin page (https://127.0.0.1:8000/admin). This page
provides a thin user interface over the database.  
1. Click on the Site approvals link.
2. Click ADD SITE APPROVAL in the top right of the page.  
3. From the drop-down list, select the only option (example.com)
4. You may also want to check the require approval box. This setting only allows users who have been 
approved by an admin user (by checking the approval box for the user using the admin site) to access The Annotator.
5. Click save

Returning to https://127.0.0.1:8000 show a "No Documents" page which is expected since no 
documents have been loaded into the system

### 9. Loading New Documents
Data can be imported (and exported) from the database as JSON objects. Thus, to add new documents
we need to create a JSON file in the correct format containing the documents we want to be annotated.
The document JSON object consists of a list of JSON objects, for example:
```
[
    {"model": "annotate_document.documentsegment", 
    "pk": 1, 
    "fields": {"title": "Test Document", 
        "body": "Shaka kaSenzangakhona (c. July 1787 – 22 September 1828), also known as Shaka Zulu and Sigidi kaSenzangakhona, was the founder of the Zulu Kingdom from 1816 to 1828.", 
        "tokens": "", 
        "part": 0, 
        "total_parts": 1, 
        "num_annotations": 0, 
        "num_assignments": 0, 
        "num_lines": 2, 
        "words": 27}
    }
]
```
The fields for each document object are as follows:
* title: The title for the document.
* body: the main document text.
* tokens: This field can be set to the empty string.
* part: if the text has been split into segments, this indicates from which segment the text was taken.
* total\_parts: The total number of segments the original document was split into.
* num\_annotations: How many users have annotated the document.
* num\assignments: How many people has the document been assigned to.
* num\_lines: How many lines should the text be displayed as in The Annotator.
* words: The number of words in the body.

Create a directory mastersannotator/annotate\_document/fixtures.
After placing the example shown above in a file mastersannotator/annotate\_document/fixtures/sample\_doc.json.
It can be loaded into the database by running the following command:
 
```
mastersannotator$ python manage.py loaddata annotate_document/fixtures/sample_doc.json 
Installed 1 object(s) from 1 fixture(s)
```
Restarting the server and going to https://127.0.0.1:8000 should now show the document ready to be
annotated.

### 10. Adding New Entities
If you select a mention from the new document (for example, Shaka kaSenzengakhona) and click highlight;
you will notice that there are currently no entities. You could allow users to create the 
entities, or you could load an initial set of entities in the same way as for the documents.
Except that an entity object has the following form:
```
[
    {"model": "annotate_document.entity", 
        "fields": {"id": 1,
            "name": "Shaka", 
            "description": "Shaka was a Zulu cheif", 
            "fhya_id": "FHYA-Shaka"
        }
    }
]
```
The fields for each entity are:
* id: A unique identifier.
* name: The person's name.
* description: A description of the person.
* fhya\_id: a human-readable id.
* date\_of\_birth: Optional date of birth.
* date\_of\_death: Optional date of death.

As when loading documents if the example above was saved in mastersannotator/annotate\_document/fixtures/sample\_entity.json.
It could be loaded into the database by running:
```
mastersannotator$ python manage.py loaddata annotate_document/fixtures/sample_entity.json 
Installed 1 object(s) from 1 fixture(s)
```
Restarting the server and going to https://127.0.0.1:8000 should now show the entity when linking a name.

### 11. Configurations
Most of the configurations for The Annotator can be set in mastersannotator/annotator/settings/baseline.py.  
This includes:
* WORDS\_TO\_ANNOTATE = 2500 [Initial number of words assigned to each user.]
* MAX\_SEGMENT\_LENGTH = 350 [Segment length]
* MIN\_SEGMENT\_LENGTH = 100 [Minimum segment length]
* MAX\_LINE\_LEN = 100 [Maximum number of characters per line]
* DOCUMENT\_TTL = 3 * 3600 [Number of seconds for which a document will be assigned to a user before being reallocated. If the user had not started annotating the document.]
* DOCUMENT\_INCOMPLETE\_TTL = 5 * 24 * 3600 [Number of seconds for which a document will be assigned to a user before being reallocated. If the user had started annotating the document.]
* USER\_ACTIVE = 3600 [Seconds beyond which users will be considered to be inactive.]

For the document reallocation, a cron job was used. The cron job was used to run the following script every 4 hours:
```
#!/bin/bash

source /Users/jwddunn/Documents/UCT/Masters/2021/Disseration/annotator-django/bin/activate
python /Users/jwddunn/Documents/UCT/Masters/2021/Disseration/annotator-django/annotator/manage.py cleanassignments
deactivate
```

Clean assignments removes inactive documents to prevent them from getting stuck with users who are no longer using the system.
It also ensures that the counts for num\_annotated for each document are correct.

### 12. Signup Questions
A set of questions were used to check if users could understand isiZulu. These questions need to be added
otherwise, the signup page will not work. To add the questions, run:
```
mastersannotator$ python manage.py loaddata annotate_document/fixtures/verify_language.json 
Installed 12 object(s) from 1 fixture(s)
```

### 13. Editing the Banner
There is a banner reminding users that the annotation process will close on 1 January. 
The banner can be removed or edited in **mastersannotator/templates/annotate\_document/base.html**.

## Admin Features
There are several features that are only available to admin users. These can be used to export the data and payment information.  
*For the following section, it's assumed that The Annotator is running locally at `https://127.0.0.1`*

## Export Labels
The labels can be exported for either single documents or all documents at once. In both cases, the labels are exported as JSON objects which can be saved.  

### Export Labels for One Document
Going to `https://127.0.0.1/export/<document_id>`
This produces a JSON object with the following fields:
* id: Document id.
* tokens: Tokens from the text.
* ner_tags: IOB tags for each token based on the majority vote. A single token may contain multiple tags.
* all_ner_tags: IOB tags produced by each of the annotators.
* el_tags: Entity ids for each token.
* all_el_tags: Entity ids for each token produced by each of the users.  

An example of the JSON object for a document is shown below:
```json
{
  "id": 3, 
  "tokens": ["Shaka", "was", "a", "Zulu", "chief"], 
  "ner_tags": [["I"], ["O"], ["O"], ["O"], ["O"]], 
  "all_ner_tags": [[["I"], ["O"], ["O"], ["O"], ["O"]], [["I"], ["O"], ["O"], ["O"], ["O"]], [["I"], ["O"], ["O"], ["O"], ["O"]]], 
  "el_tags": [[1], [], [], [], []], 
  "all_el_tags": [[1], [], [], [], []], [1], [], [], [], []], [1], [], [], [], []]]
}
```

### Export Labels for All Documents
To export labels for all documents go to: `https://127.0.0.1/export`
This produces a JSON object consisting of a list of JSON objects that are the same as the above.

## Payments
Payments can be exported by going to `https://127.0.0.1/payments`.  
The JSON object consists of a dictionary where the key is the user\_id and the value is a JSON object describing the links they created. The fields have the following fields:
* user: User id.
* links: Number of correct links created by the user.
* phone: User phone number.
* entries: List of objects for each link created:
  * user\_entity: Entity selected by the user.
  * max\_entity: Entity selected by the majority of users.
  * max\_links: Number of links from the mention to the max\_entity.
  * total\_links: Total number of links created for the mention.
* assigned\_document: Number of people the document was assigned to.
* num\_words: The total number of words the user has been assigned.
* unannotated: Number of times the documents has been assigned but not annotated.
* annotated: Number of times the document has been annotated.
* annotated\_words: Number of words from the documents the user has annotated.


```json
"1": {
  "user": 1, 
  "links": 2, 
  "phone": "+27121231234", 
  "entries": [
    {
      "user_entity": "Piet Retief (487)", 
      "max_entity": ["Piet Retief (487)"], 
      "max_links": 2, 
      "total_links": 3
    }, 
    {
      "user_entity": "Piet Retief (487)", 
      "max_entity": ["Piet Retief (487)"], 
      "max_links": 2, 
      "total_links": 3 
     }
  ], 
  "assigned_document": 3, 
  "num_words": 350, 
  "unannotated": 0, 
  "annotated": 3, 
  "annotated_words": 350
}
```
## View
The view page allows admin users to view annotated documents as they would appear to a particular user. The page can be viewed by going to `https://127.0.0.1/view/<user_id>/<document_id>`.  
* <user_id>: The id of the user.
* <document_id>: The id for the document.
Thus to see how the user with id = 10 would see the document with id = 12 you would go to: `https://127.0.0.1/view/10/12`.
